﻿
#include <stdio.h>
#include <assert.h>
#include <iostream>
#include <cstdint>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "ConcurrentStreamParams.cuh"
#include "cuda_helpers.cuh"

__global__
void cuda_print(unsigned int n) {
    for (unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; i < n; i += blockDim.x * gridDim.x)
        printf("Kernel Count: %d\n", i);
}

int main()
{
    CudaHelpers::print_all_device_properties();

    cuda_print <<< 2, 2 >>> (10);
    cudaDeviceSynchronize();

    return 0;
}